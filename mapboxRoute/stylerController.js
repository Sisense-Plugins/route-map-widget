mod.controller('stylerController', ['$scope',
    function ($scope) {


        /**
         * variables
         */

        //  Save the widget id (handle new widgets)
        $scope.widgetId = $scope.widget.oid ? $scope.widget.oid : 'new';


        /**
         * watches
         */
        $scope.$watch('widget', function (val) {

            //  Get a reference to the list of basemaps
            $scope.basemapOptions = settings.basemaps;

            //  Set the model, from the style
            $scope.model = $$get($scope, 'widget.style');
        });

        /**
        * public methods
        */

        $scope.redraw = function(){
            
            $scope.widget.redraw();
        }
		 
        $scope.changeBasemap = function (basemap) {

            //apply changes
            $scope.widget.style.basemap = basemap.value;
            $scope.widget.style.basemapLayer = basemap.layer;

            //  TODO: Reset the basemap on the widget
            var mapObj = $$get(prism.mapboxglwidget, $scope.widgetId, null);
            if (mapObj) {

                //  Function to reload the layer
                function styleLoaded(){
                    $scope.widget.redraw();
                }

                //  Set the event handler
                mapObj.map.on('style.load', styleLoaded);

                //  Set the new map style
                mapObj.map.setStyle(basemap.value);
            }
        };

        $scope.setRouteType = function(value){

            //apply changes
            $scope.widget.style.useArcs = value;

            //  Draw the map again
            $scope.widget.redraw();
        }

        $scope.setValueLabels = function(value){

            //apply changes
            $scope.widget.style.valueLabels = value;

            //  Draw the map again
            $scope.widget.redraw();
        }

        $scope.setAnimation = function(value){

            //  Apply the setting
            $scope.widget.style.animation = value;

            //  Draw the map again
            $scope.widget.redraw();
        }
    }
]);