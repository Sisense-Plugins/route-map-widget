
//  Panel name defintion
var panels = {
    origin: {
        lat: "Origin Latitude",
        lng: "Origin Longitude"
    },
    destination: {
        lat: "Destination Latitude",
        lng: "Destination Longitude"
    },
    label: "Route Label",
    values: "Values"
}

prism.registerWidget("mapboxroutewidget", {
    name: "mapboxroutewidget",
    family: "map",
    title: "Route Map",
    iconSmall: "/plugins/mapboxRoute/icon-24.png",
    styleEditorTemplate: "/plugins/mapboxRoute/styler.html",
    hideNoResults: true,
    style: {
        basemapLayer: "housenum-label",
        showControl: true,
        controlLocation: "top-right",
        zoomLevel: 2,
        basemap: "mapbox://styles/mapbox/basic-v9",
        valueLabels: 'none',
        useArcs: true,
        lineWidth: 2,
        padding: 20,
        arcSteps: 10,
        animation: false,
        animationSpeed: 1
    },
    directive: {
        desktop: "mapboxroutewidget"
    },
    data: {
        selection: [],
        defaultQueryResult: {},
        panels: [
            {
                name: panels.origin.lat,
                type: 'visible',
                metadata: {
                    types: ['dimensions'],
                    maxitems: 1
                },
                visibility: true
            },
            {
                name: panels.origin.lng,
                type: 'visible',
                metadata: {
                    types: ['dimensions'],
                    maxitems: 1
                },
                visibility: true
            },{
                name: panels.destination.lat,
                type: 'visible',
                metadata: {
                    types: ['dimensions'],
                    maxitems: 1
                },
                visibility: true
            },
            {
                name: panels.destination.lng,
                type: 'visible',
                metadata: {
                    types: ['dimensions'],
                    maxitems: 1
                },
                visibility: true
            },
            {
                name: panels.label,
                type: 'visible',
                metadata: {
                    types: ['dimensions'],
                    maxitems: 1
                },
                visibility: true
            },
            {
                name: panels.values,
                type: "visible",
                metadata: {
                    types: ['measures'],
                    maxitems: 5
                },
                itemAttributes: ["color"],
                allowedColoringTypes: {
                    color: true,
                    condition: true,
                    range: true
                },
                visibility: true
            },
            {
                name: 'filters',
                type: 'filters',
                metadata: {
                    types: ['dimensions'],
                    maxitems: -1
                }
            }
        ],
        //  Allow coloring for the value panel
        canColor: function (widget, panel, item) {
            return (panel.name === panels.values);
        },
        // builds a jaql query from the given widget
        buildQuery: function (widget) {

            // building jaql query object from widget metadata 
            var query = { 
                datasource: widget.datasource, 
                format: "json",
                isMaskedResult:true,
                metadata: [] 
            };

            //  Get the metadata panels
            var olatItems = widget.metadata.panel(panels.origin.lat).items,
                olngItems = widget.metadata.panel(panels.origin.lng).items,
                dlatItems = widget.metadata.panel(panels.destination.lat).items,
                dlngItems = widget.metadata.panel(panels.destination.lng).items,
                labelItems = widget.metadata.panel(panels.label).items,
                valItems = widget.metadata.panel(panels.values).items,
                filterItems = widget.metadata.panel("filters").items;

            //  Do we have all required fields?
            var isValid = (olatItems.length>0) && (olngItems.length>0) && (dlatItems.length>0) && (dlngItems.length>0) && (valItems.length>0);
            if (isValid) {

                //  Function to create a new lat/lng item
                function latLngItem(item,isLat){
                    //  Clone the old object
                    var newItem = $$.object.clone(item, true);
                    //  Add a filter to exclude invalid results: Lat must be between -90 & 90, Lng must be between -180 & 180
                    newItem.jaql.filter = {
                        ">=": isLat ? -90 : -180,
                        "<=": isLat ? 90 : 180
                    }
                    return newItem
                }

                // push origin and destination lat/lngs
                olatItems.forEach(function(item){
                    query.metadata.push(latLngItem(item));
                })
                olngItems.forEach(function(item){
                    query.metadata.push(latLngItem(item));
                })
                dlatItems.forEach(function(item){
                    query.metadata.push(latLngItem(item));
                })
                dlngItems.forEach(function(item){
                    query.metadata.push(latLngItem(item));
                })
                
                // push label Items
                labelItems.forEach(function(item){
                    
                    //  Clone the old object
                    var newItem = $$.object.clone(item, true);
                    
                    //  Look for any matching dashboard filters
                    var dashboardFilters = [];
                    widget.dashboard.filters.$$items.forEach(function(f){
                        if (f.isCascading){
                            f.levels.forEach(function(f2){
                                if(f2.dim == item.jaql.dim) {
                                    dashboardFilters = f2.filter.members;
                                }
                            })
                        } else if (f.jaql.dim == item.jaql.dim) {
                            dashboardFilters = f.jaql.filter.members;
                        }
                    })

                    //  Was there a selection?
                    if (dashboardFilters && dashboardFilters.length > 0) {

                        //  Create the filter base
                        newItem.jaql.in = {
                            'selected': {
                                'jaql': $$.object.clone(item.jaql, true)
                            }
                        }

                        //  Set the filter selection
                        newItem.jaql.in.selected.jaql.filter = {
                            'members': dashboardFilters
                        }
                    }

                    //  Save to metadata array
                    query.metadata.push(newItem);
                })

                // push Value Items
                valItems.forEach(function(item){
                    //  Save to metadata array
                    query.metadata.push(item);
                })

                // push widget filter selections
                filterItems.forEach(function(item){
                    
                    //  Create a new version of the item
                    var newItem = $$.object.clone(item, true);

                    //  Specify that this is a filter
                    newItem.panel = "scope";

                    //  Save to metadata array
                    query.metadata.push(newItem);
                })
            }
            
            return query;
        },
        //  Build geojson object from query result
        processResult : function (widget, queryResult) {

            //  Get the Sisense formatter
            var formatter = prism.$injector.get('$filter')('numeric'),
                palette = prism.$ngscope.dashboard.style.palette(),
                defaultTooltipColor = 'black',
                defaultColor = '#F6F6F6',
                defaultUnselectedColor = '#F6F6F6',
                hasData = queryResult.$$rows.length > 0,
                arcScale = widget.style.arcSteps;

            //  Get the metadata panels
            var olatItems = widget.metadata.panel(panels.origin.lat).items,
                olngItems = widget.metadata.panel(panels.origin.lng).items,
                dlatItems = widget.metadata.panel(panels.destination.lat).items,
                dlngItems = widget.metadata.panel(panels.destination.lng).items,
                labelItems = widget.metadata.panel(panels.label).items,
                valItems = widget.metadata.panel(panels.values).items,
                filterItems = widget.metadata.panel("filters").items;

            //  Is there a selected flight route?
            var hasSelection = queryResult.metadata().filter(function(i) { 
                return (i.jaql.dim == $$get(labelItems, '0.jaql.dim',null)) && (i.jaql.in); 
            }).length > 0;

            //  Check for how many items were specified
            var hasLabel = labelItems.length > 0,
                numValues = valItems.length,
                valuesIdx = hasLabel ? 5 : 4;

            //  Create an initial bounding box
            var boundingBox = {
                lat: {
                    min: $$get(queryResult.$$rows, '0.0.data'),
                    max: $$get(queryResult.$$rows, '0.0.data'),
                },
                lng: {
                    min: $$get(queryResult.$$rows, '0.1.data'),
                    max: $$get(queryResult.$$rows, '0.1.data')
                }
            }

            //  Define a function for handling calculating min/max lat/lng
            function calcBounds(lats,lngs,box){

                //  Evaluation the lats
                lats.forEach(function(lat){
                    if (lat > box.lat.max){
                        box.lat.max = lat;
                    }
                    if (lat < box.lat.min){
                        box.lat.min = lat;
                    }
                })

                //  Evaluate the lngs
                lngs.forEach(function(lng){
                    if (lng > box.lng.max){
                        box.lng.max = lng;
                    }
                    if (lng < box.lng.min){
                        box.lng.min = lng;
                    }
                })

                return box
            }
        
            //  Create the outline for a geojson object
            var geojson = {
                type: "FeatureCollection",
                features: [],
            };
            
            //  Loop through each route
            for (var i=0; i<queryResult.$$rows.length; i++) {
                
                //  Get the values from the row
                var olat = queryResult.$$rows[i][0].data,
                    olng = queryResult.$$rows[i][1].data,
                    dlat = queryResult.$$rows[i][2].data,
                    dlng = queryResult.$$rows[i][3].data,
                    label = hasLabel ? queryResult.$$rows[i][4].text : "Route " + i,
                    isSelected = hasSelection ? $$get(queryResult.$$rows[i][4], 'selected', false) : true;
                    
                //  Calculate the bounding box
                boundingBox = calcBounds([olat, dlat], [olng, dlng], boundingBox);

                //  Define the point
                var route = {
                    "type": "Feature",
                    "properties": {
                        "id": i,
                        "label": label,
                        "color": null,
                        "opacity": isSelected ? 1 : 0.15,
                        "tooltip": {}
                    },
                    "geometry": {
                        "type": "LineString",
                        "coordinates": [[olng,olat] , [dlng, dlat]]
                        }
                }

                //  Save the values to the route's properties
                for (var j=0; j<numValues; j++){

                    //  Get the value details
                    var valueKey = valItems[j].jaql.title,
                        valueData = queryResult.$$rows[i][valuesIdx+j].data,
                        valueMask = $$get(valItems[j],'format.mask', null),
                        valueColor = $$get(queryResult.$$rows[i][valuesIdx+j],'color', defaultColor);

                    //  Assign a color to the line
                    if (!route.properties.color) {
                        route.properties.color = valueColor;
                    }

                    //  Save to the route
                    route.properties.tooltip[valueKey] = {
                        'value': valueData,
                        'text': formatter(valueData,valueMask),
                        'color': valueColor ? valueColor : defaultTooltipColor
                    };
                }

                //  Should we determine an arc to use instead of straight line?
                if (widget.style.useArcs) {

                    // Calculate the distance in kilometers between route start/end point.
                    var lineDistance = turf.lineDistance(route, 'kilometers');

                    //  Init an array for the arc
                    var arcs = [ [] ],
                        arcIndex = 0,
                        lastStep = -1;

                    // Draw an arc between the `origin` & `destination` of the two points
                    for (var k = 0; k < lineDistance; k += lineDistance / 500) {

                        //  Calculate a point between the origin and destination
                        var segment = turf.along(route, k, 'kilometers');

                        //  Figure out last lat/lng points
                        var lastLng = $$get(arcs, arcIndex + '.' + lastStep + '.0', segment.geometry.coordinates[0]),
                            lastLat = $$get(arcs, arcIndex + '.' + lastStep + '.1', segment.geometry.coordinates[1]);

                        //  Calculate a safe lat/lng
                        var thisLng = segment.geometry.coordinates[0],
                            thisLat = segment.geometry.coordinates[1];

                        //  Figure out if this needs to be a new line
                        if (Math.abs(thisLng - lastLng) > 180) {
                            arcIndex += 1;
                        } else if (Math.abs(thisLat - lastLat) > 90) {
                            arcIndex += 1;
                        }

                        //  Ensure the array is initialized for this arc
                        if (typeof arcs[arcIndex] == "undefined"){
                            arcs[arcIndex] = [];
                            if (lastStep >=0){
                                
                            }
                        }

                        //  Increment the counter
                        lastStep += 1;

                        //  Save the coordinates to the proper arc
                        arcs[arcIndex].push([thisLng, thisLat]);
                    }

                    //  Loop through every line in this arc, and add the geojson
                    arcs.forEach( function(arc){

                        //  Create a copy of the existing route
                        var myRoute = $$.object.clone(route, true);

                        //  Assign in the arc for this part of the route
                        myRoute.geometry.coordinates = arc;

                        //  Save this to the array of routes
                        geojson.features.push(myRoute);
                    })
                } else {

                    //  Save to the feature
                    geojson.features.push(route);
                }
            }

            //  Define an object containing the new results
            var newResults = {
                geojson: geojson,
                box: [[boundingBox.lng.min, boundingBox.lat.min], [boundingBox.lng.max,boundingBox.lat.max]]
            };
            
            //  if we have data, return the map data
            return hasData ? newResults : null;
        }
    },
    beforequery: function(widget,args){

        //  Get the route label's metadata item
        var routeItem = $$get(widget.metadata.panel(panels.label), 'items.0',null);
        if (routeItem){

            //  Filter out metadata items that are filters AND match the route label's dimension
            var newMeta = args.query.metadata.filter(function(item){
                var isMatch = (item.panel == "scope") && (item.jaql.dim == routeItem.jaql.dim);
                return !isMatch;
            })
            args.query.metadata = newMeta;
        }
    },
    render : function (widget, event) {

        //  Helper function to calculate a point on a line
        function getLinePoint(line, location){

            //  Figure out the start/end points
            var origin = line.geometry.coordinates[0].slice(),
                destination = line.geometry.coordinates[1].slice();

            //  Business Logic
            if (location == "origin"){

                //  Return the first point's lat/lng
                return origin;
            } else if (location == "destination"){

                //  Return the last point's lat/lng
                return destination;
            } else if (location == "middle"){

                //  Calculate a point between the origin and destination
                var middle = turf.midpoint(turf.point(origin), turf.point(destination))

                //  Return just the lat/lng
                return middle.geometry.coordinates.slice();
            }
        }

        //  Function to get the map objects
        function getMap(){

            //  Define the widget id (handle new widgets)
            var widgetId = widget.oid ? widget.oid : 'new';

            //  Find the map
            return $$get(prism.mapboxglwidget, widgetId, null);
        }

        //  Function to get the bottom layer for this map style
        function getBottomLayer(){

            //  Find the selected basemap object
            var selection = settings.basemaps.filter(function(m){
                return (m.value == widget.style.basemap);
            })
            
            //  return the bottom layer from config.js
            return (selection.length>0) ? selection[0].bottomLayer : '';
        }

        //  Function to handle tooltips
        function mapTooltip(event, popup){

            //  Get the map object
            var map = getMap().map;

            //  Change the cursor style as a UI indicator.
            map.getCanvas().style.cursor = 'pointer';

            //  Figure out the coordinates of this middle point
            //var coordinates = getLinePoint(event.features[0],'middle');
            var coordinates = [event.lngLat.lng, event.lngLat.lat];

            //  Define HTML to use for this popup
            var props = event.features[0].properties,
                tooltips = JSON.parse(props.tooltip);

            //  Get the label to use
            var label = props.label ? props.label : props.id;
            var htmlDesc = '<div class="mapboxgl-tooltip-container">'
                            + '<div class="mapboxgl-tooltip-label">'
                                + label
                            + '</div>'
                            + '<div class="mapboxgl-tooltip-values">';

            //  Loop through and find all values
            for (key in tooltips){

                //  Make sure this is something we want to show
                var isValid = (tooltips.hasOwnProperty(key));
                if (isValid) {

                    //  Defne html for this value
                    var valueHtml = '<div class="mapboxgl-tooltip-value" style="color: ' + tooltips[key].color + ';">'
                                        + key + ': ' + tooltips[key].text
                                    + '</div>'

                    //  Add to the existing thml
                    htmlDesc = htmlDesc + valueHtml
                }
            }

            //  Close the html divs
            htmlDesc = htmlDesc + '</div></div>';

            // Ensure that if the map is zoomed out such that multiple
            // copies of the feature are visible, the popup appears
            // over the copy being pointed to.
            while (Math.abs(event.lngLat.lng - coordinates[0]) > 180) {
                coordinates[0] += event.lngLat.lng > coordinates[0] ? 360 : -360;
            }

            // Populate the popup and set its coordinates
            // based on the feature found.
            popup.setLngLat(coordinates)
                .setHTML(htmlDesc)
                .addTo(map);
        }

        //  Function for point selection
        function routeSelected(event){

            //  Set the options
            var options = {
                save:true, 
                refresh:true, 
                unionIfSameDimensionAndSameType:false
            }

            //  Get the item to filter on
            filterItem = $$get(widget.metadata.panel(panels.label), 'items.0',null);

            //  Get the selected route
            var route = $$get(event.features[0],'properties.label',null);

            if (route && filterItem){

                //  Create the base object
                var filter = {
                    'jaql': $$.object.clone(filterItem.jaql,true)
                }
                //  Add the filter selection
                filter.jaql.filter = {
                    'explicit': true,
                    'multiSelection':false,
                    'members': [route]
                }
                filter.jaql.collapsed = true;

                //  Set the filter with JS
                widget.dashboard.filters.update(filter,options)
            }
        }

        //  Function to ensure the map has loaded (race condition)
        function checkForMap(widget){

            //  Find the map
            var mapObj = getMap(),
                mapLoaded = widget.options.mapLoaded;

            if (mapLoaded){
                //  Found the map, ready to start
                loadMap(mapObj, widget);
            } else {
                //  Map not loaded
                setTimeout(function(){
                    checkForMap(widget)
                }, 500)
            }
        }

        //  Function to clear and load the map layers
        function loadMap(mapObj, widget){

            //  Get the objects
            var map = mapObj.map,
                control = mapObj.control,
                resetControl = mapObj.resetControl,
                filterItem = $$get(widget.metadata.panel(panels.label), 'items.0',null);

            //  define the source ids
            var sourceId = 'elasticubeData',
                routeLayer = 'layer-routes',
                bottomLayer = getBottomLayer(),
                zoomLevel = widget.style.zoomLevel;

            /***************************************/    
            /*  Remove any existing data/layers    */
            /***************************************/

            //  Remove the old layers, if they were already added
            if (map.getLayer(routeLayer)){
                map.removeLayer(routeLayer);
            }
            
            //  Remove the old source, if there is one
            if (map.getSource(sourceId)){
                map.removeSource(sourceId);
            }

            //  Only add layers/sources if the query returned data
            var hasData = !$.isEmptyObject(widget.queryResult);
            if (hasData) {

                /********************************/    
                /*  Add Data Source & Layers    */
                /********************************/

                //  Define the dataset to load
                var data = {
                    type: 'geojson',
                    data: widget.queryResult.geojson
                }

                //  add the data to the map
                map.addSource(sourceId, data);

                // add route layer here
                map.addLayer(getRouteLayer(routeLayer,sourceId,widget.style.lineWidth), bottomLayer);


                /********************/    
                /*  Map Controls    */
                /********************/

                //  Safely add the control
                map.removeControl(control);
                map.addControl(control, widget.style.controlLocation);
                if (filterItem){
                    map.removeControl(resetControl);
                    map.addControl(resetControl, 'top-left');
                }

                //  Add filter selections
                if (filterItem){
                    map.on('click', routeLayer, routeSelected);
                }

                /****************/    
                /*  Tooltips    */
                /****************/

                // Create a popup, but don't add it to the map yet.
                var popup = new mapboxgl.Popup({
                    closeButton: false,
                    closeOnClick: false
                });

                //  Add handler for tooltips
                map.on('mouseenter', routeLayer, function(event){
                    mapTooltip(event, popup);
                });

                //  Additional handler for hover tooltips
                map.on("mouseleave", routeLayer, function(){
                    map.getCanvas().style.cursor = '';
                    popup.remove();
                });
            
                
                /******************/    
                /*  Last Steps    */
                /******************/

                //  Define the fit options
                var fitOptions = {
                    maxZoom: settings.zoom.max,
                    padding: {
                        top: widget.style.padding,
                        bottom: widget.style.padding,
                        left: widget.style.padding,
                        right: widget.style.padding
                    }
                }

                //  Center the map around the data points
                map.fitBounds(widget.queryResult.box,fitOptions);
            }

            //  Notify Sisense that the widget is done loading (for PDF printing)
            widget.trigger('domready', widget);
        }

        //  Start the process
        checkForMap(widget);
    },
    destroy : function (widget, args) {},
    options: {
        dashboardFiltersMode: "slice",
        selector: false,
        title: false
    },
    sizing: {
        minHeight: 128, //header
        maxHeight: 2048,
        minWidth: 128,
        maxWidth: 2048,
        height: 320,
        defaultWidth: 512
    }
});

/*************************/
/*  Layering Functions   */
/*************************/


//  Layer for clusters
function getRouteLayer(id, source, lineWidth){

    return {
        "id": id,
        "source": source,
        "type": "line",
        "layout": {
            "line-cap": "round",
            "line-join": "round"
        },
        "paint": {
            "line-width": lineWidth,
            "line-color": ["get", "color"],
            "line-opacity": ["get","opacity"]
        }
    }
}


/************************/
/*  Utility Functions   */
/************************/

function rgbToHex(r, g, b) {
  return "#" + ((1 << 24) + (r << 16) + (g << 8) + b).toString(16).slice(1);
}

function hexToRgb(hex) {
  var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
  return result
    ? {
        r: parseInt(result[1], 16),
        g: parseInt(result[2], 16),
        b: parseInt(result[3], 16)
      }
    : null;
}

// returns an array of startColor, colors between according to steps, and endColor
function getRamp(startColor, endColor, steps) {
  var ramp = [];

  ramp.push(startColor);

  var startColorRgb = hexToRgb(startColor);
  var endColorRgb = hexToRgb(endColor);

  var rInc = Math.round((endColorRgb.r - startColorRgb.r) / (steps+1));
  var gInc = Math.round((endColorRgb.g - startColorRgb.g) / (steps+1));
  var bInc = Math.round((endColorRgb.b - startColorRgb.b) / (steps+1));

  for (var i = 0; i < steps; i++) {
    startColorRgb.r += rInc;
    startColorRgb.g += gInc;
    startColorRgb.b += bInc;

    ramp.push(rgbToHex(startColorRgb.r, startColorRgb.g, startColorRgb.b));
  }
  ramp.push(endColor);

  return ramp;
}

function logslider(position,count) {
  // position will be between 0 and 100
  var minp = 0;
  var maxp = count;

  // The result should be between 100 an 10000000
  var minv = Math.log(20);
  var maxv = Math.log(99.9);

  // calculate adjustment factor
  var scale = (maxv-minv) / (maxp-minp);

  return (Math.exp(minv + scale*(position-minp)) / 100);
}

