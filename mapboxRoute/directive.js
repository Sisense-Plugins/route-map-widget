mod.directive('mapboxroutewidget', [

    function ($timeout, $dom) {

        return {
            priority: 0,
            replace: false,
            templateUrl: "/plugins/mapboxRoute/template.html",
            transclude: false,
            restrict: 'E',
            link: function ($scope, element, attrs) {

                /************************************/
                /*  Map Formatting Handlers         */
                /************************************/

                //  Function for when the widget gets resized
                function mapResized(widget, args, scope){
                    
                    //  Look for the map object, corresponding to this widget
                    var map = $scope.map;
                    if (map) {
                        //  Resize the map
                        map.resize();
                    }
                }

                /************************************/
                /*  Functions for point selection   */
                /************************************/

                //  Function for setting/resetting a filter based on lat/lng
                function setFilter(bbox){

                    //  User must have a route label in order to filter
                    if ($scope.filterItem) {

                        //  Set the options
                        var options = {
                            save:true, 
                            refresh:true, 
                            unionIfSameDimensionAndSameType:false
                        }

                        //  Create the filter jaql
                        var filter = {
                            'jaql': $$.object.clone($scope.filterItem.jaql,true)
                        }

                        //  Set the filter selection
                        filter.jaql.filter = {
                            'collapsed': true,
                            'all': true
                        }

                        //  Set the filters through the API
                        $scope.dashboard.filters.update(filter, options)
                    }
                }

                //  Custom Mapbox Control for Resetting filters
                class clearFilterControl {
                  onAdd(map){

                    //  Set the map
                    this.map = map;
                    
                    //  Create HTML elements
                    var myButtonGroup = $('<div class="mapboxgl-ctrl mapboxgl-ctrl-group"></div>'),
                        myButton = $('<button class="clear-filter-button" type="button" aria-label="Reset Filters">Reset Filters</button>');

                    //  Add event handler
                    myButton.on('click',function(){
                        setFilter(null)
                    });

                    //  append button to group
                    myButtonGroup.append(myButton);

                    //  Assign as the container
                    this.container = myButtonGroup[0];

                    return this.container;
                  }
                  onRemove(){
                    this.container.parentNode.removeChild(this.container);
                    this.map = undefined;
                  }
                }

                /************************************/
                /*  Init the Map                    */
                /************************************/

                //  Get the widget id
                $scope.widgetId = $scope.widget.oid ? $scope.widget.oid : 'new';

                //  Get a reference to the dimension used for filtering
                $scope.filterItem = $$get($scope.widget.metadata.panel('Route Label'), 'items.0',null);

                //  Wait until after the template has loaded
                setTimeout( function(){

                    //  Init the flag, so that the map is marked as "not loaded yet"
                    $scope.widget.options.mapLoaded = false;
                    
                    //  Define the access token for mpabox
                    mapboxgl.accessToken = settings.apiToken;

                    //  define the div container's id
                    var divId = 'mapboxgl-' + $scope.widgetId;

                    //  Define the 
                    var map = new mapboxgl.Map({
                        container: divId,
                        style: $scope.widget.style.basemap,
                        zoom: 2,
                        center: [-73.987697, 40.751759]
                    });

                    //  Set the flag within the widget, to mark the map as done initializing
                    map.on('load', function() {
                        
                        //  Add the standard navigation  control
                        var control = new mapboxgl.NavigationControl();
                        var controlOptions = {};
                        map.addControl(control, $scope.widget.style.controlLocation);

                        //  Add custom control for resetting filters
                        if ($scope.filterItem) {
                            var resetControl = new clearFilterControl();
                            map.addControl(resetControl, 'top-left');
                        }

                        //  Save the control to the widget
                        prism.mapboxglwidget[$scope.widgetId].control = control;
                        prism.mapboxglwidget[$scope.widgetId].resetControl = resetControl;

                        //  Init variables for point selection
                        $scope.selector = {
                            start: null,    //  Start mouse location
                            current: null,  //  Current mouse location
                            box: null,      //  Bounding box
                            canvas: map.getCanvasContainer()    //  map canvas container
                        }
                        $scope.map = map;

                        //  Mark widget loaded flag
                        $scope.widget.options.mapLoaded = true;

                        //  Add handler for
                        $scope.widget.off('readjust', mapResized)
                        $scope.widget.on('readjust', mapResized)
                    })

                    //  Save a reference to the map object, within the widget's options
                    if (typeof prism.mapboxglwidget === "undefined") {
                        prism.mapboxglwidget = {}
                    }
                    prism.mapboxglwidget[$scope.widgetId] = {
                        map: map,
                        control: null
                    };
    
                },0)
            }
        }
    }]);

