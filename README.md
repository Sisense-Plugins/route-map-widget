#	MapboxGL - Route Map

__INTRODUCTION__: This project is for a Sisense plugin, which generates a new widget type for point to point routing using MapboxGL.  This map takes in and origin and destination (as lat/long), and draws a line connecting them on the map.

![Widget Editor](screenshots/dashboard.png)

__IMPLEMENTATION__: 

*Step 1:* Download the attached .zip file, and extract it to the following path "[Sisense installation path]\PrismWeb\Plugins". 

*Step 2:* Open the included file, *config.js*, and add your mapbox API token in the *apiToken* setting.  For more information on how to obtain a mapbox token, please use the following links

https://www.mapbox.com/help/how-access-tokens-work/

https://www.mapbox.com/api-documentation/#tokens

*Step 3:* On your dashboard, click the *Create Widget* button and select *Advanced Configuration*. Next, select *Route Map* from the dropdown.  In the data tab, there are several panels for adding data.

* Origin Latitude(required): The latitude field for the starting point (must be a number)
* Origin Longitude(required): The longitude field for the starting point (must be a number)
* Destination Latitude(required): The latitude field for the ending point (must be a number)
* Destination Longitude(required): The longitude field for the ending point (must be a number)
* Route Label(required for filtering): If used, this is the label that will show up within the tooltip for each point
* Value(1 required): One or more measures to calculate per point.  The first value added is also used to determine the coloring of the points

*Step 4:* Coloring

The color of each line is based on the first value's color.  You can select a single color, a range of color values, or a conditional color.  All other values' colors will be used for tooltip coloring.

![Coloring](screenshots/coloring.png)

*Step 5:* Formatting

![Making a filter selection](screenshots/widget-editor.png)

* Basemap: This is the background to use for each widget.  We've included the default public map styles, but you can add to this by editing the *config.js* file.  For more information on creating your own basemaps, check out Mapbox's documentation site
https://www.mapbox.com/help/create-a-custom-style/
* Line Type: You can choose between straight and curved lines
* Line Width: How thick each line appears
* Bounding Box Padding: This map will automatically fit itself based on the provided origin/destinations. This setting allows you to control how much spacing is added around this calculated bounding box.

*Step 6:* Using the widget
* You can you your mouse to drag across the map, to pan across your data
* Double clicking on the map zooms in, and you can also use the scroll wheel of your mouse
* In order to make filter selections, you must specify a Route Label.  Once you add this, the map will allow users to click on a route to make a selection.  A reset button will also appear if a Route Label has been added.

![Making a filter selection](screenshots/filter-selected.png)

__NOTES__: 
* This sample has been confirmed working on Sisense version 7.0.1, and should be backwards compatible with previous version.
* There is a known limitation with this plugin, as it does not support export to PDF